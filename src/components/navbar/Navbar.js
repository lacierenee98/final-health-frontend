import React from 'react';
import { NavLink } from 'react-router-dom';
import './NavBar.css';

/**
 * @name NavBar
 * @description selectively show links for pages
 * @return component
 */
const NavBar = () => (
  <nav className="navbar">
    <h3 className="navbar-name">Health Inc. </h3>
    <NavLink to="/" className="link" activeclassname="active">
      Home
    </NavLink>

    <NavLink to="/patients" className="link" activeclassname="active">
      Patients
    </NavLink>
  </nav>
);

export default NavBar;
