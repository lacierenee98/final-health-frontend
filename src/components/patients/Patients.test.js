/* eslint-disable */
import Patients from './Patients.js';
import Button from '../button/Button.js';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';
import { fireEvent, render, getByTestId, getByDisplayValue } from '@testing-library/react';
import renderer from 'react-test-renderer';
import { fetchPatients } from './Patients.js'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter> <Patients /> </BrowserRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('shows current patients header', () => {
  const { getByText } = render(<BrowserRouter> <Patients /> </BrowserRouter>);
  expect(getByText('Current Patients')).toBeInTheDocument();
});

it('patients component matches snapshot', () => {
  const component = renderer.create(<BrowserRouter> <Patients /> </BrowserRouter>);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});




