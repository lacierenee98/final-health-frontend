import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../button/Button';
import './Patients.css';

/**
 * @name Patients
 * @returns component
 */
const Patients = ({ apiError, setApiError }) => {
  // Hook to work with URL
  const navigate = useNavigate();

  // State for patients
  const [patients, setPatients] = useState([]);

  // State for encounters (to check if patient has encounters before delete)
  const [encounters, setEncounters] = useState([]);

  // State for delete error if a patient has encounters
  const [deleteError, setDeleteError] = useState('');

  // Fetch initialization
  const init = {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      mode: 'cors'
    })
  };

  /**
     * Fetch GET all patients wrapped in a useEffect hook.
     * Sets the patients array in state so that the page is
     * automatically populated with all patients from the DB.
     */
  useEffect(() => {
    const fetchPatients = async () => {
      await fetch('http://localhost:8080/patients', init)
        .then((res) => {
          if (!res.ok) {
            throw Error('Oops something went wrong');
          }
          return res.json();
        })
        .then((data) => {
          setPatients(data);
        })
        .catch(() => {
          setApiError(true);
        });
    };
    fetchPatients();
  }, []);

  /**
     * Fetch GET all encounters wrapped in a useEffect hook.
     * Sets the encounters array in state for the checkDelete function
     * which checks if the patient has associated encounters
     */
  useEffect(() => {
    const fetchAllEncounters = async () => {
      await fetch('http://localhost:8080/patients/encounters', init)
        .then((res) => {
          if (!res.ok) {
            throw Error('Oops something went wrong');
          }
          return res.json();
        })
        .then((data) => {
          setEncounters(data);
        })
        .catch(() => {
          setApiError(true);
        });
    };
    fetchAllEncounters();
  }, []);

  /**
     * Async fetch function to delete a patient by their id, filters
     * out deleted patients so they won't be displayed on the page and
     * sets the delete error to blank
     * @param {*} id - id of patient to be deleted
     */
  const deletePatient = async (id) => {
    await fetch(`http://localhost:8080/patients/${id}`, {
      method: 'DELETE',
      headers: new Headers({
        'Content-Type': 'application/json',
        mode: 'cors'
      })
    });
    setPatients(patients.filter((patient) => patient.id !== id));
    setDeleteError('');
  };

  /**
     * Takes the patient id from whatever patient was clicked on,
     * and checks if that patient id is listed in any of the encounters. If
     * not, the patient is deleted. If the patient does have associated encounters,
     * an error message is displayed.
     * @param {*} id - patient id
     */
  const checkDelete = (id, firstName, lastName) => {
    let pEncounters = 0;
    for (let i = 0; i < encounters.length; i++) {
      if (id === encounters[i].patientId) {
        pEncounters += 1;
      }
    }
    if (pEncounters > 0) {
      setDeleteError(`Cannot delete patient with encounters: ${firstName} ${lastName}`);
    } else {
      pEncounters = 0;
      deletePatient(id);
    }
  };

  // Map patients array to a list of buttons
  const patientList = patients.map((patient) => (
    <li className="patient-item" key={patient.id}>
      <Button
        name="patient-btn"
        onClick={() => navigate(`/patient-details/${patient.id}`)}
      >
        {`${patient.firstName} ${patient.lastName}`}
        {' | '}
        {' '}
        {`${patient.age}`}
        {' | '}
        {' '}
        {`${patient.gender}`}
      </Button>
      <Button name="delete-patient" onClick={() => checkDelete(patient.id, patient.firstName, patient.lastName)}>Delete</Button>
    </li>
  ));

  return (
    <div className="patients-page">
      <h5 data-testid="errMsg">{apiError ? 'Something went wrong. Cannot fetch data' : ''}</h5>
      <h1 className="current-patients">Current Patients</h1>
      <div className="patients-list">
        <ul className="patient-list" data-testid="patient-list">
          {patientList}
        </ul>
      </div>
      <h5 className="delete-patient-err">{deleteError}</h5>
      <div>
        <Button data-testid="add-patient-btn" name="add-patient" onClick={() => navigate('/patients/add-patient')}>Add Patient</Button>
      </div>
    </div>
  );
};

export default Patients;
