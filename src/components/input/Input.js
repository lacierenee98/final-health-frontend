import React from 'react';
import './Input.css';

/**
 * @name Input
 * @description styling wrapper around html input element
 * @param {*} props error, errMsg, name, type, value, onChange
 * @return component
 */
const Input = (props) => {
  const {
    error, errMsg, name, type, value, onChange
  } = props;

  return (
    <div className="input-container">
      <label className="input-label" htmlFor={name}>{name}</label>

      <input
        className={`${'input'} ${error ? 'error' : ''}`}
        name={name}
        type={type}
        value={value}
        onChange={onChange}
      />

      {error && <p className="error-msg">{errMsg}</p>}
    </div>
  );
};

export default Input;
