/* eslint-disable */
import Input from './Input';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';
import { fireEvent, render, getByTestId } from '@testing-library/react';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Input />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('input component matches snapshot', () => {
  const component = renderer.create(<Input />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});