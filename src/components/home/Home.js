import React from 'react';
import './Home.css';

/**
 * @name Home
 * @returns component
 */
const Home = () => (
  <h1 className="home-header">This is the home page!</h1>
);

export default Home;
