import React, { useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Patients from '../patients/Patients';
import NotFound from '../not-found/NotFound';
import './App.css';
import AddPatient from '../add-patient/AddPatient';
import PatientDetails from '../patient-details/PatientDetails';
import NavBar from '../navbar/Navbar';
import Home from '../home/Home';
import EditPatient from '../edit-patient/EditPatient';
import AddEncounter from '../add-encounter/AddEncounter';
import EditEncounter from '../edit-encounter/EditEcounter';
import EncounterDetails from '../encounter-details/EncounterDetails';

/**
 * @name App
 * @returns component
 */
const App = () => {
  /* Hooks for apiError, passed as props to each 
  component that fetches data */
  const [apiError, setApiError] = useState(false);

  return (
    <div className="App">
      <BrowserRouter>
        <NavBar />
        <div className="Routes">
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/patients" element={<Patients apiError={apiError} setApiError={setApiError} />} />
            <Route exact path="/patients/add-patient" element={<AddPatient apiError={apiError} setApiError={setApiError} />} />
            <Route exact path="/patient-details/:id" element={<PatientDetails apiError={apiError} setApiError={setApiError} />} />
            <Route exact path="/patient-details/:id/edit-patient/" element={<EditPatient apiError={apiError} setApiError={setApiError} />} />
            <Route exact path="/patient-details/:pid/encounter-details/:eid" element={<EncounterDetails apiError={apiError} setApiError={setApiError} />} />
            <Route exact path="/patient-details/:id/add-encounter" element={<AddEncounter apiError={apiError} setApiError={setApiError} />} />
            <Route exact path="/patient-details/:pid/edit-encounter/:eid" element={<EditEncounter apiError={apiError} setApiError={setApiError} />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
};

export default App;
