import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import Button from '../button/Button';
import './PatientDetails.css';

/**
 * @name PatientDetails
 * @returns component
 */
const PatientDetails = ({ apiError, setApiError }) => {
  // Hooks to work with URL
  const navigate = useNavigate();
  const { id } = useParams();

  // State for loading
  const [isLoading, setIsLoading] = useState(true);

  // State for patient
  const [patient, setPatient] = useState({
    id: null,
    firstName: '',
    lastName: '',
    ssn: '',
    email: '',
    street: '',
    city: '',
    state: '',
    postal: '',
    age: 0,
    height: 0,
    weight: 0,
    insurance: '',
    gender: ''
  });

  // State for patient encounters
  const [encounters, setEncounters] = useState([]);

  // Fetch initialization
  const init = {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      mode: 'cors'
    })
  };

  /**
     * Fetch GET a patient by id to display, wrapped in a useEffect hook.
     * Sets the patient field values in state so that the page is
     * automatically populated with selected patient values.
     */
  useEffect(() => {
    const fetchPatient = async () => {
      await fetch(`http://localhost:8080/patients/${id}`, init)
        .then((res) => {
          if (!res.ok) {
            throw Error('Oops something went wrong');
          }
          return res.json();
        })
        .then((data) => {
          setPatient(data);
        })
        .catch(() => {
          setApiError(true);
        });
    };
    fetchPatient();
    setIsLoading(false);
  }, []);

  /**
     * Fetch GET all encounters for a patient id to display, wrapped in a useEffect hook.
     * Sets the encounter field values in state so that the page is
     * automatically populated with selected encounter values.
     */
  useEffect(() => {
    const fetchEncounters = async () => {
      await fetch(`http://localhost:8080/patients/${id}/encounters`, init)
        .then((res) => {
          if (!res.ok) {
            throw Error('Oops something went wrong');
          }
          return res.json();
        })
        .then((data) => {
          setEncounters(data);
        })
        .catch(() => {
          setApiError(true);
        });
    };
    fetchEncounters();
  }, []);

  // Map encounters to a list
  const encounterList = encounters.map((encounter) => (
    <li className="encounter" key={encounter.id}>
      {`ID: ${encounter.id}`}
      <br />
      {`Visit Code: ${encounter.visitCode}`}
      <br />
      {`Provider: ${encounter.provider}`}
      <br />
      {`Date: ${encounter.date}`}
      <br />
      <Button name="view-encounter" onClick={() => navigate(`/patient-details/${patient.id}/encounter-details/${encounter.id}`)}>View</Button>
    </li>
  ));

  return (
    <div className="patient-details-page">
      <h5>{apiError ? 'Something went wrong. Cannot fetch data' : ''}</h5>
      <h1>Patient Details</h1>
      <div className="details-lists">
        <ul className="patient-details">
          <li
            className="patient-details-header"
          >
            {isLoading ? '' : `Patient ID: ${patient.id}`}
            <Button name="edit-patient" onClick={() => navigate(`/patient-details/${patient.id}/edit-patient`)}>Edit</Button>
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Name: `} &nbsp; &nbsp; &nbsp;&nbsp; {`${patient.firstName} ${patient.lastName}`} </div>}
            <br/>
          </li>
          <li>
            {' '}
            {isLoading ? '' : <div> {`SSN: `}  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {`${patient.ssn}`} </div>}
            <br/>
          </li>
          <li>
            {' '}
            {isLoading ? '' : <div> {`Email: `} &nbsp; &nbsp; &nbsp; &nbsp; {`${patient.email}`} </div>}
            <br/>
          </li>
          <li>
            {' '}
            {isLoading ? '' : <div> {`Address: `}&nbsp; &nbsp; {`${patient.street}`} </div>}
            {' '}
          </li>
          <li>
            {' '}
            {isLoading ? '' :  <div className='address2'>&nbsp; &nbsp; {`${patient.city}, ${patient.state} ${patient.postal}`} </div>}
            
            <br/>
          </li>
          <li>
            {' '}
            {isLoading ? '' : <div> {`Age: `}&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;{`${patient.age}`} </div>}
            <br/>
          </li>
          <li>
            {' '}
            {isLoading ? '' : <div> {`Height: `}&nbsp; &nbsp; &nbsp;&nbsp;{`${patient.height}`} </div>}

            <br/>
          </li>
          <li>
            {' '}
            {isLoading ? '' : <div> {`Weight: `}&nbsp; &nbsp; &nbsp;{`${patient.weight}`} </div>}
            <br/>          </li>
          <li>
            {' '}
            {isLoading ? '' : <div> {`Insurance: `}&nbsp;{`${patient.insurance}`} </div>}
            <br/>
          </li>
          <li>
            {' '}
            {isLoading ? '' : <div> {`Gender: `}&nbsp; &nbsp; &nbsp;{`${patient.gender}`} </div>}
            <br/>
          </li>
        </ul>
        <ul className="patient-encounters">
          <li
            className="patient-details-header"
          >
            Encounters
            <Button name="add-encounter" onClick={() => navigate(`/patient-details/${patient.id}/add-encounter`)}>Add</Button>
          </li>
          <br />
          {encounterList}
        </ul>
      </div>
    </div>
  );
};
export default PatientDetails;
