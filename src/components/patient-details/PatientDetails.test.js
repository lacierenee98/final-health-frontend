/* eslint-disable */
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import PatientDetails from './PatientDetails.js';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter> <PatientDetails /> </BrowserRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('patient details component matches snapshot', () => {
  const component = renderer.create(<BrowserRouter> <PatientDetails /> </BrowserRouter>);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
