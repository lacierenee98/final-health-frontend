import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Form from '../form/Form';
import Input from '../input/Input';
import {
  isValidName, isValidSsn, isValidEmail, isValidState,
  isValidZip, isValidNumber, isValidGender, whiteSpaces
} from '../utils/Validation';

/**
 * @name EditPatient
 * @returns component
 */
const EditPatient = ({ apiError, setApiError }) => {
  // Hooks to work with URL
  const navigate = useNavigate();
  const { id } = useParams();

  // State for loading
  const [isLoading, setIsLoading] = useState(true);
  
  //State for patients to check if email already in use
  const [patients, setPatients] = useState([]);

  // State for patient
  const [patient, setPatient] = useState({
    id: null,
    firstName: '',
    lastName: '',
    ssn: '',
    email: '',
    street: '',
    city: '',
    state: '',
    postal: '',
    age: null,
    height: null,
    weight: null,
    insurance: '',
    gender: ''
  });

  // State to track input errors
  const [inputErrors, setInputErrors] = useState({
    firstName: false,
    lastName: false,
    ssn: false,
    email: false,
    street: false,
    city: false,
    state: false,
    postal: false,
    age: false,
    height: false,
    weight: false,
    insurance: false,
    gender: false
  });

  // Fetch initialization
  const init = {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      mode: 'cors'
    })
  };

  /**
     * Fetch GET a patient by id to edit, wrapped in a useEffect hook.
     * Sets the patient field values in state so that the form is
     * automatically populated with existing patient values.
     */
  useEffect(() => {
    const fetchPatient = async () => {
      await fetch(`http://localhost:8080/patients/${id}`, init)
        .then((res) => {
          if (!res.ok) {
            throw Error('Oops something went wrong');
          }
          return res.json();
        })
        .then((data) => {
          setPatient(data);
        })
        .catch(() => {
          setApiError(true);
        });
    };
    fetchPatient();
    setIsLoading(false);
  }, []);

  /**
     * Fetch GET all patients to check emails, wrapped in a useEffect hook.
     */
  useEffect(() => {
    const fetchPatients = async () => {
      await fetch(`http://localhost:8080/patients`, init)
        .then((res) => {
          if (!res.ok) {
            throw Error('Oops something went wrong');
          }
          return res.json();
        })
        .then((data) => {
          setPatients(data);
        })
        .catch(() => {
          setApiError(true);
        });
    };
    fetchPatients();
  }, []);

  /**
     * Checks patient email that was input in the form against the emails
     * of patients already in the database
     * @returns pEmail - number
     */
  const isEmailInUse = () => {
    let pEmail = 0;
    for (let i = 0; i < patients.length; i++) {
      if (patient.email === patients[i].email) {
        pEmail += 1;
      }
    }
    return pEmail;
  };

  /**
     * Fetch PUT a patient by id, updates the field values
     * based on input changes
     * @param {*} patientDetails - all patient fields
     * @returns
     */
  const editPatient = async (patientDetails) => fetch(`http://localhost:8080/patients/${id}`, {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json',
      mode: 'cors'
    }),
    body: JSON.stringify(patientDetails)
  })
    .then((data) => data.json())
    .catch(() => {
      setApiError(true);
    });

  /**
     * Handles form submission, validates all fields. On successful submission,
     * navigates the user back to the patient details page.
     * @param {*} event
     */
  const handleSubmit = async (event) => {
    event.preventDefault();
    const errors = {
      firstName: false,
      lastName: false,
      ssn: false,
      email: false,
      street: false,
      city: false,
      state: false,
      postal: false,
      age: false,
      height: false,
      weight: false,
      insurance: false,
      gender: false
    };

    let formValid = true;
    let emailInUse = isEmailInUse();

    if ((patient.firstName.length === 0) || (!isValidName(patient.firstName))) {
      errors.firstName = true;
      formValid = false;
    }

    if ((patient.lastName.length === 0) || (!isValidName(patient.lastName))) {
      errors.lastName = true;
      formValid = false;
    }

    if ((patient.ssn.length === 0) || (!isValidSsn(patient.ssn))) {
      errors.ssn = true;
      formValid = false;
    }

    if ((patient.email.length === 0) || (!isValidEmail(patient.email)) || (emailInUse > 0)) {
      errors.email = true;
      formValid = false;
    }

    if ((patient.street.length === 0) || (whiteSpaces(patient.street))) {
      errors.street = true;
      formValid = false;
    }

    if ((patient.city.length === 0) || (whiteSpaces(patient.city))) {
      errors.city = true;
      formValid = false;
    }

    if ((patient.state.length === 0) || (!isValidState(patient.state))) {
      errors.state = true;
      formValid = false;
    }

    if ((patient.postal.length === 0) || (!isValidZip(patient.postal))) {
      errors.postal = true;
      formValid = false;
    }

    if ((patient.age === null) || (!isValidNumber(patient.age))) {
      errors.age = true;
      formValid = false;
    }

    if ((patient.height === null) || (!isValidNumber(patient.height))) {
      errors.height = true;
      formValid = false;
    }

    if ((patient.weight === null) || (!isValidNumber(patient.weight))) {
      errors.weight = true;
      formValid = false;
    }

    if ((patient.insurance.length === 0) || (whiteSpaces(patient.insurance))) {
      errors.insurance = true;
      formValid = false;
    }

    if ((patient.gender.length === 0) || (isValidGender(patient.gender) === false)) {
      errors.gender = true;
      formValid = false;
    }

    if (formValid === true) {
      await editPatient({
        id: patient.id,
        firstName: patient.firstName,
        lastName: patient.lastName,
        ssn: patient.ssn,
        email: patient.email,
        street: patient.street,
        city: patient.city,
        state: patient.state,
        postal: patient.postal,
        age: patient.age,
        height: patient.height,
        weight: patient.weight,
        insurance: patient.insurance,
        gender: patient.gender
      });
      navigate(`/patient-details/${patient.id}`);
    } else {
      setInputErrors(errors);
    }
  };

  /**
     * Handles input changes and errors for form fields
     * @param {*} event - input change
     * @param {*} input - which input was changed
     */
  const handleChange = (event, input) => {
    if (inputErrors[input]) {
      setInputErrors({ ...inputErrors, [input]: false });
    }
    setPatient({ ...patient, [input]: event.target.value });
  };

  return (
    <div className="edit-patient-page">
      <h5>{apiError ? 'Something went wrong. Cannot fetch data' : ''}</h5>
      <Form title="Edit Patient" onSubmit={handleSubmit}>
        <Input
          name="First Name "
          type="text"
          value={isLoading ? '' : patient.firstName}
          error={inputErrors.firstName}
          errMsg={patient.firstName.length === 0 ? 'Must not be blank' : 'Must be valid name'}
          onChange={(event) => handleChange(event, 'firstName')}
        />
        <Input
          name="Last Name "
          type="text"
          value={isLoading ? '' : patient.lastName}
          error={inputErrors.lastName}
          errMsg={patient.lastName.length === 0 ? 'Must not be blank' : 'Must be valid name'}
          onChange={(event) => handleChange(event, 'lastName')}
        />
        <Input
          name="SSN "
          type="text"
          value={isLoading ? '' : patient.ssn}
          error={inputErrors.ssn}
          errMsg={patient.ssn.length === 0 ? 'Must not be blank' : 'Must be valid ssn XXX-XX-XXXX'}
          onChange={(event) => handleChange(event, 'ssn')}
        />
        <Input
          name="Email "
          type="text"
          value={isLoading ? '' : patient.email}
          error={inputErrors.email}
          errMsg={patient.email.length === 0 ? 'Must not be blank' : 'Invalid email or email already taken'}
          onChange={(event) => handleChange(event, 'email')}
        />
        <Input
          name="Street "
          type="text"
          value={isLoading ? '' : patient.street}
          error={inputErrors.street}
          errMsg="Must not be blank"
          onChange={(event) => handleChange(event, 'street')}
        />
        <Input
          name="City "
          type="text"
          value={isLoading ? '' : patient.city}
          error={inputErrors.city}
          errMsg="Must not be blank"
          onChange={(event) => handleChange(event, 'city')}
        />
        <Input
          name="State "
          type="text"
          value={isLoading ? '' : patient.state}
          error={inputErrors.state}
          errMsg={patient.state.length === 0 ? 'Must not be blank' : 'Must be valid state abbreviaton'}
          onChange={(event) => handleChange(event, 'state')}
        />
        <Input
          name="Zip Code "
          type="text"
          value={isLoading ? '' : patient.postal}
          error={inputErrors.postal}
          errMsg={patient.postal.length === 0 ? 'Must not be blank' : 'Must be valid zip code XXXXX or XXXXX-XXXX'}
          onChange={(event) => handleChange(event, 'postal')}
        />
        <Input
          name="Age "
          type="text"
          value={isLoading ? '' : patient.age}
          error={inputErrors.age}
          errMsg={patient.age === null ? 'Must not be blank' : 'Must be valid number'}
          onChange={(event) => handleChange(event, 'age')}
        />
        <Input
          name="Height "
          type="text"
          error={inputErrors.height}
          value={isLoading ? '' : patient.height}
          errMsg={patient.height === null ? 'Must not be blank' : 'Must be valid number'}
          onChange={(event) => handleChange(event, 'height')}
        />
        <Input
          name="Weight "
          type="text"
          value={isLoading ? '' : patient.weight}
          error={inputErrors.weight}
          errMsg={patient.weight === null ? 'Must not be blank' : 'Must be valid number'}
          onChange={(event) => handleChange(event, 'weight')}
        />
        <Input
          name="Insurance "
          type="text"
          value={isLoading ? '' : patient.insurance}
          error={inputErrors.insurance}
          errMsg="Must not be blank"
          onChange={(event) => handleChange(event, 'insurance')}
        />
        <Input
          name="Gender "
          type="text"
          value={isLoading ? '' : patient.gender}
          error={inputErrors.gender}
          errMsg={patient.gender.length === 0 ? 'Must not be blank' : 'Must be Male, Female, or Other'}
          onChange={(event) => handleChange(event, 'gender')}
        />
      </Form>
    </div>
  );
};

export default EditPatient;
