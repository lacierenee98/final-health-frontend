/* eslint-disable */
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import EditPatient from './EditPatient.js';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter> <EditPatient /> </BrowserRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('edit patient component matches snapshot', () => {
  const component = renderer.create(<BrowserRouter> <EditPatient /> </BrowserRouter>);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
