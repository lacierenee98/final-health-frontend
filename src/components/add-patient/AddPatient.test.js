/* eslint-disable */
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import AddPatient from './AddPatient';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter> <AddPatient /> </BrowserRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('add patient component matches snapshot', () => {
  const component = renderer.create(<BrowserRouter> <AddPatient /> </BrowserRouter>);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
