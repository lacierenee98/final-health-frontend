import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import Button from '../button/Button';

import './EncounterDetails.css';

/**
 * @name EncounterDetails
 * @returns component
 */
const EncounterDetails = ({ apiError, setApiError }) => {
  // Hooks to work with URL
  const navigate = useNavigate();
  const { pid, eid } = useParams();

  // State for loading
  const [isLoading, setIsLoading] = useState(true);

  // State for encounter
  const [encounter, setEncounter] = useState({
    id: null,
    patientId: pid,
    notes: '',
    visitCode: '',
    provider: '',
    billingCode: '',
    icd10: '',
    totalCost: null,
    copay: null,
    chiefComplaint: '',
    pulse: null,
    systolic: null,
    diastolic: null,
    date: ''
  });

  // Fetch initialization
  const init = {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      mode: 'cors'
    })
  };

  /**
     * Fetch GET an encounter by id to display, wrapped in a useEffect hook.
     * Sets the encounter field values in state so that the page is
     * automatically populated with selected encounter values.
     */
  useEffect(() => {
    const fetchEncounter = async () => {
      await fetch(`http://localhost:8080/patients/${pid}/encounters/${eid}`, init)
        .then((res) => {
          if (!res.ok) {
            throw Error('Oops something went wrong');
          }
          return res.json();
        })
        .then((data) => {
          setEncounter(data);
        })
        .catch(() => {
          setApiError(true);
        });
    };
    fetchEncounter();
    setIsLoading(false);
  }, []);

  return (
    <div className="patient-details-page">
      <h5>{apiError ? 'Something went wrong. Cannot fetch data' : ''}</h5>
      <h1>{`Encounter Details for Patient #${pid}`}</h1>
      <div className="encounters-list">
        <ul className="patient-details">
          <li
            className="patient-details-header"
          >
            {isLoading ? '' : `Encounter ID: ${encounter.id}`}
            <Button name="edit-encounter" onClick={() => navigate(`/patient-details/${pid}/edit-encounter/${eid}`)}>Edit</Button>
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Notes: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{`${encounter.notes}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Visit Code: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {`${encounter.visitCode}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Provider: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{`${encounter.provider}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Billing Code: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{`${encounter.billingCode}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`ICD10 Code: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{`${encounter.icd10}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Total Cost: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;{`$${encounter.totalCost}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Copay: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;{`$${encounter.copay}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Chief Complaint: `} &nbsp; {`${encounter.chiefComplaint}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Pulse: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {`${encounter.pulse}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' :  <div> {`Systolic: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {`${encounter.systolic}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Diastolic: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{`${encounter.diastolic}`} </div>}
            {' '}
          </li>
          <br />
          <li>
            {' '}
            {isLoading ? '' : <div> {`Date: `} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{`${encounter.date}`} </div>}
            {' '}
          </li>
          <br />
        </ul>
      </div>
    </div>
  );
};
export default EncounterDetails;
