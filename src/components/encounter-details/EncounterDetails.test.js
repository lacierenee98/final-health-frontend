/* eslint-disable */
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import EncounterDetails from './EncounterDetails.js';
import renderer from 'react-test-renderer';
import { fireEvent, render, getByTestId } from '@testing-library/react';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter> <EncounterDetails /> </BrowserRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('encounter details component matches snapshot', () => {
  const component = renderer.create(<BrowserRouter> <EncounterDetails /> </BrowserRouter>);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
