import React from 'react';

/**
 * @name Button
 * @description Styling wrapper around html button
 * @param {*} props children, onClick, name, type
 * @return component
 */
const Button = (props) => {
  const {
    children, onClick, name, type
  } = props;

  return <button className={name} type={type} onClick={onClick}>{children}</button>;
};

export default Button;
