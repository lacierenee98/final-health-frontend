import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Form from '../form/Form';
import Input from '../input/Input';
import {
  isValidBillingCode, isValidDate, isValidIcd10, isValidNumber,
  isValidVisitCode, whiteSpaces
} from '../utils/Validation';

/**
 * @name EditEncounter
 * @returns component
 */
const EditEncounter = ({ apiError, setApiError }) => {
  // Hooks to work with URL
  const navigate = useNavigate();
  const { pid, eid } = useParams();

  // State for loading
  const [isLoading, setIsLoading] = useState(true);

  // State for encounter
  const [encounter, setEncounter] = useState({
    id: null,
    patientId: pid,
    notes: '',
    visitCode: '',
    provider: '',
    billingCode: '',
    icd10: '',
    totalCost: null,
    copay: null,
    chiefComplaint: '',
    pulse: null,
    systolic: null,
    diastolic: null,
    date: ''
  });

  // State to track input errors
  const [inputErrors, setInputErrors] = useState({
    visitCode: '',
    provider: '',
    billingCode: '',
    icd10: '',
    totalCost: null,
    copay: null,
    chiefComplaint: '',
    date: ''
  });

  // Fetch initialization
  const init = {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      mode: 'cors'
    })
  };

  /**
     * Fetch GET an encounter by id to edit, wrapped in a useEffect hook.
     * Sets the encounter field values in state so that the form is
     * automatically populated with existing encounter values.
     */
  useEffect(() => {
    const fetchEncounter = async () => {
      await fetch(`http://localhost:8080/patients/${pid}/encounters/${eid}`, init)
        .then((res) => {
          if (!res.ok) {
            throw Error('Oops something went wrong');
          }
          return res.json();
        })
        .then((data) => {
          setEncounter(data);
        })
        .catch(() => {
          setApiError(true);
        });
    };
    fetchEncounter();
    setIsLoading(false);
  }, []);

  /**
     * Fetch PUT an encounter by id, updates the field values
     * based on input changes
     * @param {*} encounterDetails - all encounter fields
     * @returns
     */
  const updateEncounter = async (encounterDetails) => fetch(`http://localhost:8080/patients/${pid}/encounters/${eid}`, {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json',
      mode: 'cors'
    }),
    body: JSON.stringify(encounterDetails)
  })
    .then((data) => data.json())
    .catch(() => {
      setApiError(true);
    });

  /**
     * Handles form submission, validates all fields. On successful submission,
     * navigates the user back to the encounter details page.
     * @param {*} event
     */
  const handleSubmit = async (event) => {
    event.preventDefault();
    const errors = {
      visitCode: false,
      provider: false,
      billingCode: false,
      icd10: false,
      totalCost: false,
      copay: false,
      chiefComplaint: false,
      pulse: false,
      systolic: false,
      diastolic: false,
      date: false
    };

    let formValid = true;

    if ((encounter.visitCode.length === 0) || (!isValidVisitCode(encounter.visitCode))) {
      errors.visitCode = true;
      formValid = false;
    }

    if ((encounter.provider.length === 0) || (whiteSpaces(encounter.provider))) {
      errors.provider = true;
      formValid = false;
    }

    if ((encounter.billingCode.length === 0) || (!isValidBillingCode(encounter.billingCode))) {
      errors.billingCode = true;
      formValid = false;
    }

    if ((encounter.icd10.length === 0) || (!isValidIcd10(encounter.icd10))) {
      errors.icd10 = true;
      formValid = false;
    }

    if (((encounter.totalCost === '') && (encounter.totalCost !== 0)) || (!isValidNumber(encounter.totalCost))) {
      errors.totalCost = true;
      formValid = false;
    }

    if (((encounter.copay === '') && (encounter.copay !== 0)) || (!isValidNumber(encounter.copay))) {
      errors.copay = true;
      formValid = false;
    }

    if ((encounter.chiefComplaint.length === 0) || (whiteSpaces(encounter.chiefComplaint))) {
      errors.chiefComplaint = true;
      formValid = false;
    }

    if ((encounter.pulse !== null) && (!isValidNumber(encounter.pulse))) {
      errors.pulse = true;
      formValid = false;
    }

    if ((encounter.systolic !== null) && (!isValidNumber(encounter.systolic))) {
      errors.systolic = true;
      formValid = false;
    }

    if ((encounter.diastolic !== null) && (!isValidNumber(encounter.diastolic))) {
      errors.diastolic = true;
      formValid = false;
    }

    if ((encounter.date.length === 0) || (!isValidDate(encounter.date))) {
      errors.date = true;
      formValid = false;
    }

    if (formValid === true) {
      await updateEncounter({
        id: eid,
        patientId: pid,
        notes: encounter.notes,
        visitCode: encounter.visitCode,
        provider: encounter.provider,
        billingCode: encounter.billingCode,
        icd10: encounter.icd10,
        totalCost: encounter.totalCost,
        copay: encounter.copay,
        chiefComplaint: encounter.chiefComplaint,
        pulse: encounter.pulse,
        systolic: encounter.systolic,
        diastolic: encounter.diastolic,
        date: encounter.date
      });
      navigate(`/patient-details/${pid}/encounter-details/${eid}`);
    } else {
      setInputErrors(errors);
    }
  };

  /**
     * Handles input changes and errors for form fields
     * @param {*} event - input change
     * @param {*} input - which input was changed
     */
  const handleChange = (event, input) => {
    if (inputErrors[input]) {
      setInputErrors({ ...inputErrors, [input]: false });
    }
    setEncounter({ ...encounter, [input]: event.target.value });
  };

  return (
    <div className="add-encounter-page">
      <h5>{apiError ? 'Something went wrong. Cannot fetch data' : ''}</h5>
      <Form title="Edit Encounter" onSubmit={handleSubmit}>
        <Input
          name="Notes "
          type="textarea"
          value={isLoading || encounter.notes === null ? '' : encounter.notes}
          onChange={(event) => handleChange(event, 'notes')}
        />
        <Input
          name="Visit Code "
          type="text"
          value={isLoading ? '' : encounter.visitCode}
          error={inputErrors.visitCode}
          errMsg={encounter.visitCode.length === 0 ? 'Must not be blank' : 'Must be valid visit code'}
          onChange={(event) => handleChange(event, 'visitCode')}
        />
        <Input
          name="Provider "
          type="text"
          value={isLoading ? '' : encounter.provider}
          error={inputErrors.provider}
          errMsg="Must not be blank"
          onChange={(event) => handleChange(event, 'provider')}
        />
        <Input
          name="Billing Code "
          type="text"
          value={isLoading ? '' : encounter.billingCode}
          error={inputErrors.billingCode}
          errMsg={encounter.billingCode.length === 0 ? 'Must not be blank' : 'Must be valid billing code'}
          onChange={(event) => handleChange(event, 'billingCode')}
        />
        <Input
          name="ICD 10 "
          type="text"
          value={isLoading ? '' : encounter.icd10}
          error={inputErrors.icd10}
          errMsg={encounter.icd10.length === 0 ? 'Must not be blank' : 'Must be valid ICD10 code'}
          onChange={(event) => handleChange(event, 'icd10')}
        />
        <Input
          name="Total Cost "
          type="text"
          value={isLoading ? '' : encounter.totalCost}
          error={inputErrors.totalCost}
          errMsg={encounter.totalCost === '' ? 'Must not be blank' : 'Must be a number'}
          onChange={(event) => handleChange(event, 'totalCost')}
        />
        <Input
          name="Copay "
          type="text"
          value={isLoading ? '' : encounter.copay}
          error={inputErrors.copay}
          errMsg={encounter.copay === '' ? 'Must not be blank' : 'Must be a number'}
          onChange={(event) => handleChange(event, 'copay')}
        />
        <Input
          name="Chief Complaint "
          type="text"
          value={isLoading ? '' : encounter.chiefComplaint}
          error={inputErrors.chiefComplaint}
          errMsg="Must not be blank"
          onChange={(event) => handleChange(event, 'chiefComplaint')}
        />
        <Input
          name="Pulse "
          type="text"
          value={isLoading || encounter.pulse === null ? '' : encounter.pulse}
          error={inputErrors.pulse}
          errMsg="Must be a valid number"
          onChange={(event) => handleChange(event, 'pulse')}
        />
        <Input
          name="Systolic "
          type="text"
          value={isLoading || encounter.systolic === null ? '' : encounter.systolic}
          error={inputErrors.systolic}
          errMsg="Must be a valid number"
          onChange={(event) => handleChange(event, 'systolic')}
        />
        <Input
          name="Diastolic "
          type="text"
          value={isLoading || encounter.diastolic === null ? '' : encounter.diastolic}
          error={inputErrors.diastolic}
          errMsg="Must be a valid number"
          onChange={(event) => handleChange(event, 'diastolic')}
        />
        <Input
          name="Date "
          type="text"
          value={isLoading ? '' : encounter.date}
          error={inputErrors.date}
          errMsg={encounter.date.length === 0 ? 'Must not be blank' : 'Must be a valid date YYYY-MM-DD'}
          onChange={(event) => handleChange(event, 'date')}
        />
      </Form>
    </div>
  );
};

export default EditEncounter;
