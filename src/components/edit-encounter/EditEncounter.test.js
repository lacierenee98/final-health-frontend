/* eslint-disable */
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import EditEncounter from './EditEcounter.js';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter> <EditEncounter /> </BrowserRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('edit encounter component matches snapshot', () => {
  const component = renderer.create(<BrowserRouter> <EditEncounter /> </BrowserRouter>);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
