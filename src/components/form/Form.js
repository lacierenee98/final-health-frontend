import React from 'react';
import Button from '../button/Button';
import './Form.css';

/**
 * @name Form
 * @description styling wrapper around html form element with submit button
 * @param {*} props children, title, onsubmit
 * @return component
 */
const Form = (props) => {
  const { children, title, onSubmit } = props;

  return (
    <form className="form" noValidate onSubmit={onSubmit}>
      <h1 className="form-title">{title}</h1>
      {children}
      <Button name="submit-btn" type="submit">Submit</Button>
    </form>
  );
};

export default Form;
