/* eslint-disable */
import Form from './Form';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';
import { fireEvent, render, getByTestId } from '@testing-library/react';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Form />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('form component matches snapshot', () => {
  const component = renderer.create(<Form />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});