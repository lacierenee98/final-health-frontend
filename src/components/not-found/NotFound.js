import React from 'react';

/**
 * @name NotFound
 * @description displayed when navigating to link that does not exist
 * @return component
 */
const NotFound = () => (
  <h1>404 Page Not Found</h1>
);

export default NotFound;
