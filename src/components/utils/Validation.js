/**
 * @name isValidName
 * @description checks if first or last name is a valid format
 * @param {string} firstOrLastName - name to test against regex
 * @return boolean
 */
export const isValidName = (firstOrLastName) => /^[-'a-zA-Z]*$/.test(firstOrLastName);

/**
 * @name isValidSsn
 * @description checks if ssn is a valid format XXX-XX-XXXX
 * @param {string} ssn - ssn to test against regex
 * @return boolean
 */
export const isValidSsn = (ssn) => /^\d{3}-\d{2}-\d{4}$/.test(ssn);

/**
 * @name isValidEmail
 * @description checks if email is in valid email format
 * @param {string} email - email to test against regex
 * @return boolean
 */
export const isValidEmail = (email) => /^\S+@\S+\.\S+$/.test(email);

/**
 * @name isValidState
 * @description checks if state is a valid two letter state abbrev.
 * @param {string} state - state to test against regex
 * @return boolean
 */
export const isValidState = (state) => /^(?:(A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]))$/.test(state);

/**
 * @name isValidZip
 * @description checks if postal code is a valid format XXXXX or XXXXX-XXXX
 * @param {string} postal - name to test against regex
 * @return boolean
 */
export const isValidZip = (postal) => /^\d{5}$|^\d{5}-\d{4}$/.test(postal);

/**
 * @name isValidNumber
 * @description checks if the nummber is a valid number (no letters)
 * @param {number} number - number to test against regex
 * @return boolean
 */
export const isValidNumber = (number) => /^\d+$/.test(number);

/**
 * @name isValidGender
 * @description checks if gender is a valid option (Male, Female, or Other)
 * @param {string} gender - gender to test
 * @return boolean
 */
export const isValidGender = (gender) => {
  if ((gender === 'Male') || (gender === 'male') || (gender === 'Female') || (gender === 'female')
    || (gender === 'Other') || (gender === 'other')) {
    return true;
  }

  return false;
};

/**
 * @name whiteSpaces
 * @description checks for white spaces in input
 * @param {string} testString - string to test against regex
 * @return boolean
 */
export const whiteSpaces = (testString) => /^\s*$/.test(testString);

/**
 * @name isValidVisitCode
 * @description checks if visit code is a valid format LDL DLD
 * @param {string} visitCode - visit code to test against regex
 * @return boolean
 */
export const isValidVisitCode = (visitCode) => /([A-Z][0-9][A-Z])\ ([0-9][A-Z][0-9])$/.test(visitCode);

/**
 * @name isValidBillingCode
 * @description checks if billing code is a valid format DDD.DDD.DDD-DD
 * @param {string} billingCode - billing code to test against regex
 * @return boolean
 */
export const isValidBillingCode = (billingCode) => /([0-9]{3})\.([0-9]{3})\.([0-9]{3})\-([0-9]{2})$/.test(billingCode);

/**
 * @name isValidIcd10
 * @description checks if icd10 is a valid format LDD
 * @param {string} icd10 - icd10 to test against regex
 * @return boolean
 */
export const isValidIcd10 = (icd10) => /([A-Z][0-9][0-9])$/.test(icd10);

/**
 * @name isValidDate
 * @description checks if date is a valid format YYYY-MM-DD
 * @param {string} date - date to test against regex
 * @return boolean
 */
export const isValidDate = (date) => /[0-9]{4}-([0][0-9]|[1][0-2])-([0][0-9]|[1][0-9]|[2][0-9]|[3][0-1])$/.test(date);
