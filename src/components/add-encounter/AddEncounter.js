import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Form from '../form/Form';
import Input from '../input/Input';
import {
  isValidBillingCode, isValidDate, isValidIcd10, isValidNumber, isValidVisitCode, whiteSpaces
} from '../utils/Validation';

/**
 * @name AddEncounter
 * @returns component
 */
const AddEncounter = ({ apiError, setApiError }) => {
  // Hooks to work with URL
  const navigate = useNavigate();
  const { id } = useParams();

  // State for encounter
  const [encounter, setEncounter] = useState({
    id: null,
    patientId: id,
    notes: '',
    visitCode: '',
    provider: '',
    billingCode: '',
    icd10: '',
    totalCost: null,
    copay: null,
    chiefComplaint: '',
    pulse: null,
    systolic: null,
    diastolic: null,
    date: ''
  });

  // State to track input errors
  const [inputErrors, setInputErrors] = useState({
    visitCode: '',
    provider: '',
    billingCode: '',
    icd10: '',
    totalCost: null,
    copay: null,
    chiefComplaint: '',
    date: ''
  });

  /**
     * Uses fetch POST to add an encounter to the database
     * @param {*} encounterDetails - all encounter fields
     * @returns
     */
  const addEncounter = async (encounterDetails) => fetch(`http://localhost:8080/patients/${id}/encounters`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json',
      mode: 'cors'
    }),
    body: JSON.stringify(encounterDetails)
  })
    .then((data) => data.json())
    .catch(() => {
      setApiError(true);
    });

  /**
     * Handles the form submission and validates the fields for an
     * encounter object. On successful submission, navigates the user back to
     * the patient details page.
     * @param {*} event - form submission
     */
  const handleSubmit = async (event) => {
    event.preventDefault();
    const errors = {
      visitCode: false,
      provider: false,
      billingCode: false,
      icd10: false,
      totalCost: false,
      copay: false,
      chiefComplaint: false,
      pulse: false,
      systolic: false,
      diastolic: false,
      date: false
    };

    let formValid = true;

    if ((encounter.visitCode.length === 0) || (!isValidVisitCode(encounter.visitCode))) {
      errors.visitCode = true;
      formValid = false;
    }

    if ((encounter.provider.length === 0) || (whiteSpaces(encounter.provider))) {
      errors.provider = true;
      formValid = false;
    }

    if ((encounter.billingCode.length === 0) || (!isValidBillingCode(encounter.billingCode))) {
      errors.billingCode = true;
      formValid = false;
    }

    if ((encounter.icd10.length === 0) || (!isValidIcd10(encounter.icd10))) {
      errors.icd10 = true;
      formValid = false;
    }

    if (((encounter.totalCost === '') && (encounter.totalCost !== 0)) || (!isValidNumber(encounter.totalCost))) {
      errors.totalCost = true;
      formValid = false;
    }

    if (((encounter.copay === '') && (encounter.copay !== 0)) || (!isValidNumber(encounter.copay))) {
      errors.copay = true;
      formValid = false;
    }

    if ((encounter.chiefComplaint.length === 0) || (whiteSpaces(encounter.chiefComplaint))) {
      errors.chiefComplaint = true;
      formValid = false;
    }

    if ((encounter.pulse !== null) && (!isValidNumber(encounter.pulse))) {
      errors.pulse = true;
      formValid = false;
    }

    if ((encounter.systolic !== null) && (!isValidNumber(encounter.systolic))) {
      errors.systolic = true;
      formValid = false;
    }

    if ((encounter.diastolic !== null) && (!isValidNumber(encounter.diastolic))) {
      errors.diastolic = true;
      formValid = false;
    }

    if ((encounter.date.length === 0) || (!isValidDate(encounter.date))) {
      errors.date = true;
      formValid = false;
    }

    if (formValid === true) {
      await addEncounter({
        patientId: id,
        notes: encounter.notes,
        visitCode: encounter.visitCode,
        provider: encounter.provider,
        billingCode: encounter.billingCode,
        icd10: encounter.icd10,
        totalCost: encounter.totalCost,
        copay: encounter.copay,
        chiefComplaint: encounter.chiefComplaint,
        pulse: encounter.pulse,
        systolic: encounter.systolic,
        diastolic: encounter.diastolic,
        date: encounter.date
      });
      navigate(`/patient-details/${id}`);
    } else {
      setInputErrors(errors);
    }
  };

  /**
     * Handles input changes for form fields
     * @param {*} event - input change
     * @param {*} input - which input was changed
     */
  const handleChange = (event, input) => {
    if (inputErrors[input]) {
      setInputErrors({ ...inputErrors, [input]: false });
    }
    setEncounter({ ...encounter, [input]: event.target.value });
  };

  return (
    <div className="add-encounter-page">
      <h5>{apiError ? 'Something went wrong. Cannot fetch data' : ''}</h5>
      <Form title="Add New Encounter" onSubmit={handleSubmit}>
        <Input
          name="Notes "
          type="textarea"
          onChange={(event) => handleChange(event, 'notes')}
        />
        <Input
          name="Visit Code "
          type="text"
          error={inputErrors.visitCode}
          errMsg={encounter.visitCode.length === 0 ? 'Must not be blank' : 'Must be valid visit code'}
          onChange={(event) => handleChange(event, 'visitCode')}
        />
        <Input
          name="Provider "
          type="text"
          error={inputErrors.provider}
          errMsg="Must not be blank"
          onChange={(event) => handleChange(event, 'provider')}
        />
        <Input
          name="Billing Code "
          type="text"
          error={inputErrors.billingCode}
          errMsg={encounter.billingCode.length === 0 ? 'Must not be blank' : 'Must be valid billing code'}
          onChange={(event) => handleChange(event, 'billingCode')}
        />
        <Input
          name="ICD 10 "
          type="text"
          error={inputErrors.icd10}
          errMsg={encounter.icd10.length === 0 ? 'Must not be blank' : 'Must be valid ICD10 code'}
          onChange={(event) => handleChange(event, 'icd10')}
        />
        <Input
          name="Total Cost "
          type="text"
          error={inputErrors.totalCost}
          errMsg={encounter.totalCost === '' ? 'Must not be blank' : 'Must be a number'}
          onChange={(event) => handleChange(event, 'totalCost')}
        />
        <Input
          name="Copay "
          type="text"
          error={inputErrors.copay}
          errMsg={encounter.copay === '' ? 'Must not be blank' : 'Must be a number'}
          onChange={(event) => handleChange(event, 'copay')}
        />
        <Input
          name="Chief Complaint "
          type="text"
          error={inputErrors.chiefComplaint}
          errMsg="Must not be blank"
          onChange={(event) => handleChange(event, 'chiefComplaint')}
        />
        <Input
          name="Pulse "
          type="text"
          error={inputErrors.pulse}
          errMsg="Must be a valid number"
          onChange={(event) => handleChange(event, 'pulse')}
        />
        <Input
          name="Systolic "
          type="text"
          error={inputErrors.systolic}
          errMsg="Must be a valid number"
          onChange={(event) => handleChange(event, 'systolic')}
        />
        <Input
          name="Diastolic "
          type="text"
          error={inputErrors.diastolic}
          errMsg="Must be a valid number"
          onChange={(event) => handleChange(event, 'diastolic')}
        />
        <Input
          name="Date "
          type="text"
          error={inputErrors.date}
          errMsg={encounter.date.length === 0 ? 'Must not be blank' : 'Must be a valid date YYYY-MM-DD'}
          onChange={(event) => handleChange(event, 'date')}
        />
      </Form>
    </div>
  );
};

export default AddEncounter;
