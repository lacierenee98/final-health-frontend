/* eslint-disable */
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import AddEncounter from './AddEncounter.js';
import renderer from 'react-test-renderer';

const apiError = false;

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter> <AddEncounter /> </BrowserRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('add ecnounter component matches snapshot', () => {
  const component = renderer.create(<BrowserRouter> <AddEncounter /> </BrowserRouter>);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
