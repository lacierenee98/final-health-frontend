# Health Final Project - Front End

## Description
Navigating to [http://localhost:3000] will direct you to the home page of the site. From there you can use the navigation bar to navigate to the `Patients` page or go to [http://localhost:3000/patients].

This project contains randomly generated patients and encounters, and full CRUD functionality for patients on the front and back ends. There is full Create, Read, and Update functionality for encounters on the backend, and Create and Update functionality for encounters on the front end. 

## Prerequisites and Available Scripts

### Step 1: Clone this project locally.
### Step 2: CD into the root folder

After completion of steps 1 and 2 and once in the project directory, you can run:

### `npm install`
This will install any dependencies needed to launch the app

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run lint`
Running this from the root directory will lint the code using Eslint

### `npm test`
Running this from the root directory will run any files with test.js at the end. 
See the section called `Testing` for more info.

-------------------------------------------------------------------------------------

### Setting up the API
Clone the `Final Health Backend` from [https://gitlab.com/mtc-coc-se/aug-2021-cohort/lacie-lacks/final-health-backend]. Open the project in `IntelliJ`. Click the green arrow at the top to run the API or right click on the `AppRunner.class` and click run. To see your database in pgAdmin, first create a new database in pgAdmin. Then, open `application.yml` in the Hotel API under the `resources` folder and change the `url` to 
[url: jdbc:postgresql://localhost:5432/`YOUR DATABASE NAME HERE`]. Once that is done, you will be able to see the database tables in pgAdmin when you run the API. 

`Note:` If the API is not running when you start the react app, your data will not fetch from [http://localhost:8080]. 

## Testing

You can use `npm test` to run available tests (see `Available Scripts`). You can also run `npm test -- --coverage` to see test code coverage.

## Usage

### Linting - ESLint AirBnB
You shouldn't need to initialize the ESlint, but these are the steps in order to do so.

1. In the terminal, run:
 `npm install eslint-config-airbnb eslint-plugin-jsx-a11y typescript @typescript-eslint/parser`

2. In the root directory of your project, create a `.eslintrc` file and add the following code to it:

```
{
    "extends": [
      "react-app",
      "airbnb",
      "airbnb/hooks",
      "plugin:jsx-a11y/recommended"
     ],
    
    "plugins": ["jsx-a11y"],
    
    "rules": {
      "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
      "linebreak-style": 0,
      "comma-dangle": ["error", "never"],
      "react/prop-types": 0,
      "react/function-component-definition": 0,
      "no-useless-escape": 0
    }
}
```
3. In the package.json file, there is a section for scripts. Add the following line:
`"lint": "eslint"`

4. From the root directory, run `npm run lint`

5. If it just returns a new line, your code is error free!

### Navigation
Upon navigation to [http://localhost:3000], you will see the default home page and a navigation bar with two links labeled `Home` and `Patients`

Clicking the `Patients` link will navigate the user to [http://localhost:3000/patients], where they can see all
stored patients in the API. The user has the option to edit, delete, or create patients. See `Directory` at the bottom of this readme for a detailed list of URLs.

Wherever the user is on the site, they can click `Home` to return to [http://localhost:3000] or `Patients` to return to [http://localhost:3000/patients].

### Create, Edit, Delete - Patients
On the `Patients` page, clicking the `Add Patient` button will navigate the user to the `Add Patient` page, which displays a form for all inputs needed to create a patient. Error messages will display underneath incorrect fields. Upon successful form submission, the user is navigated back to 
[http://localhost:3000/patients], where they will see the patient they created.

Clicking on one of the patients will navigate you that specific patient's details page. Clicking the `Edit` button next to the patient ID will navigate you to that patient's edit page, which displays a form with all inputs filled in with the current patient's values. The user can change whichever fields they like, and error messages will display underneath incorrect fields. Upon successful form submission, the user is navigated back to `/patient-details/:id` where they will see the updated patient details.

Clicking the `Delete` button from [http://localhost:3000/patients] on an individual patient deletes that patient from the page and from the database. 

### Create, Edit - Encounters
On the `Patient Details` page for a specific patient, clicking the `Add` button next to `Encounters` will navigate the user to the `Add Encounter` page, which displays a form for all inputs needed to create an encounter. Error messages will display underneath incorrect fields. Upon successful form submission, the user is navigated back to `/patient-details/:id` where they will see the encounter they created.

On the `Patient Details` page for a specific patient, clicking the `View` button underneath an encounter will take the user to that specific encounter's details page. Clicking the `Edit` button on an individual encounter's details page will navigate you to the `Edit Encounter` page, which displays a form with all inputs filled in with the current encounter's values. The user can change whichever fields they like, and error messages will display underneath incorrect fields. Upon successful form submission, the user is navigated back to `/patient-details/:id/encounter-details/:id` where they will see the updated encounter.

## Directory

```
 Home                      :  [http://localhost:3000]
 Patients                  :  [http://localhost:3000/patients]
 Add Patient               :  [http://localhost:3000/patients/add-patient]
 Patient Details           :  [http://localhost:3000/patient-details/:id] 
 Edit Patient              :  [http://localhost:3000/patient-details/:id/edit-patient]
 Add Encounter             :  [http://localhost:3000/patient-details/:id/add-encounter]
 Encounter Details         :  [http://localhost:3000/patient-details/:pid/encounter-details/:eid]
 Edit Encounter            :  [http://localhost:3000/patient-details/:pid/edit-encounter/:eid]
 Not Found                 :  [http://localhost:3000/purchases] purchases does not exist

```
